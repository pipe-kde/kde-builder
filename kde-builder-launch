#!/usr/bin/env python3

import os
import json
import argparse
import textwrap

if __name__ == "__main__":
    
    parser = argparse.ArgumentParser(
        prog="kde-builder-launch",
        description="Run KDE applications built with kde-builder",
        epilog=textwrap.dedent("""
        EXAMPLES
        
        kde-builder-launch -f kate -l 5 file1.txt
        
        Launch kate in a new session with '-l 5 file1.txt' arguments.
        
        kde-builder-launch -e kate-syntax-highlighter kate --list-themes
        
        Launch kate-syntax-highlighter of module kate with '--list-themes' argument.
        """),  # todo fix new lines
        usage="%(prog)s [options] <module-name> [arguments]",
    )
    
    parser.add_argument("--list-installed", action="store_true", help="Print installed modules and exit.")
    parser.add_argument("-e", "--exec", nargs=1, help="Specify program of the module. Default to module name.")
    parser.add_argument("-f", "--fork", action="store_true", help="Launch the program in a new session.")
    parser.add_argument("-q", "--quiet", action="store_true", help="Do not print run information.")
    parser.add_argument("module_name_and_args", default=None, nargs=argparse.REMAINDER)
    
    args = parser.parse_args()
    
    module = args.module_name_and_args[0] if args.module_name_and_args else None
    exec_file = args.exec[0] if args.exec else module
    
    if not args.list_installed and not args.module_name_and_args or args.list_installed and any([args.exec, args.fork, args.quiet, args.module_name_and_args]):
        parser.print_help()
        exit(1)
    
    # According to XDG spec, if $XDG_STATE_HOME is not set, then we should default
    # to ~/.local/state
    xdgStateHome = os.environ.get("XDG_STATE_HOME", os.environ.get("HOME") + "/.local/state")
    dataFileName = "kdesrc-build-data"
    possibleDataPaths = ["./." + dataFileName, xdgStateHome + "/" + dataFileName]
    buildDataFile = None
    for possibleDataPath in possibleDataPaths:
        if os.path.exists(possibleDataPath):
            buildDataFile = possibleDataPath
            break
    
    if not buildDataFile:
        print(f"{dataFileName} file is not available. Exit now.")
        exit(1)
    
    with open(buildDataFile) as f:
        buildData = json.load(f)
    
    if args.list_installed:
        for key in list(buildData.keys()):
            if "install-dir" in buildData[key]:
                print(key)
        exit(0)
    
    if module not in buildData:
        print(f"Module {module} has not been built yet.")
        exit(1)
    
    buildDir = buildData[module]["build-dir"]
    installDir = buildData[module]["install-dir"]
    revision = buildData[module]["last-build-rev"]
    execPath = f"{installDir}/bin/{exec_file}"
    
    if not os.path.exists(execPath):
        print(f"Program \"{exec_file}\" does not exist.")
        print("Try to set executable name with -e option.")
        exit(127)  # Command not found
    
    # Most of the logic is done by Perl, so the shell script here should be POSIX
    # compliant. Consider using ShellCheck to make sure of that. # Todo is this message still needed?
    script = textwrap.dedent(f"""\
    #!/bin/sh
    
    # Set up environment variables (dot command)
    . "{buildDir}/prefix.sh";
    
    # Launch the program with optional arguments.
    if [ "{int(args.fork)}" = 1 ]; then
        setsid -f "{execPath}" "$@";
    else
        "{execPath}" "$@";
    fi;
    """)
    
    module_args = args.module_name_and_args[1:]
    
    # Print run information
    if not args.quiet:
        print("#" * 80)
        print(" " * 35 + "kde-builder-launch")
        print(f"Module:             {module}")
        print(f"Program:            {exec_file}")
        print(f"Revision:           {revision}")
        print(f"Arguments:          {module_args}")
        print("#" * 80)
        print()
    
    # Instead of embedding module_args in shell script with string interpolation, pass
    # them as arguments of the script. Let the shell handle the list through "$@",
    # so it will do the quoting on each one of them.
    #
    # Run the script with sh options specification:
    #     sh -c command_string command_name $1 $2 $3...
    os.execv("/bin/sh", ["/bin/sh", "-c", script, exec_file] + module_args)
